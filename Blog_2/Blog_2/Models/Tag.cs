﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        public string Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

    }
}