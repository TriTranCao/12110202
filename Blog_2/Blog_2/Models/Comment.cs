﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string Body { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdated { get; set; }
        public string Author { get; set; }

        
        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}