﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    [Table("PostBaiViet")]
    public class Post
    {
        public int ID { get; set; }
        [Required]
        public string Title { get; set; }
        [StringLength(250,ErrorMessage="so luong ky tu 10 -250",MinimumLength=10)]
        public string Body { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateUpdated { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

    }
}