namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PostBaiViet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.PostBaiViet", "Body", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PostBaiViet", "Body", c => c.String());
            AlterColumn("dbo.PostBaiViet", "Title", c => c.String());
        }
    }
}
