namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "PostBaiViet");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.PostBaiViet", newName: "Posts");
        }
    }
}
