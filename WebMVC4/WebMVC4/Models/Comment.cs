﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace WebMVC4.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public string Body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayUpDated { get; set; }
        public string Author { get; set; }
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DayCreated).Minutes;
            }
        }
        public int PostID { get; set; }

        public virtual Post Post { get; set; }
    }
}
