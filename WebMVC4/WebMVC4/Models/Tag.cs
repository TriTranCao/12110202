﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace WebMVC4.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Không để trống mục này")]
        [StringLength(100, ErrorMessage = "mục này không để trống", MinimumLength = 10)]
        public string Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}
