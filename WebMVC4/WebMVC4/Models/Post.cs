﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebMVC4.Models
{
    public class Post
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "mục này không đượ bỏ trống")]
        [StringLength(500, ErrorMessage = "nhập ký tự trong khoảng 20-500", MinimumLength = 20)]
        public string title { get; set; }
        [Required(ErrorMessage = "mục này không được để trống")]
        [StringLength(int.MaxValue, ErrorMessage = "nhập không quá 50 ký tự", MinimumLength = 50)]
        public string body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DayUpdated { get; set; }
        public string AccountID { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}