﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Post
    {
        public int ID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DateCreate { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

    }
}