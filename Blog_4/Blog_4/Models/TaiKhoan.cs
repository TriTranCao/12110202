﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_4.Models
{
    public class Account
    {
        public string TenTaiKhoan { set; get; }
        public string MatKhau { set; get; }
        public string HoTen { set; get; }
        public string Email { set; get; }
        public string ChucVu { set; get; }
        public DateTime NgaySinh { set; get; }
    }
}