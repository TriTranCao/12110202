﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_4.Models
{
    public class Idea
    {
        public string NoiDung { set; get; }

        public virtual ICollection<Account> Accounts { set; get; }
    }
}