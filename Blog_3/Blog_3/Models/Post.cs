﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Post
    {
        public int ID { set; get; }
        [StringLength(500, ErrorMessage = "so luong ky tu 20 -500", MinimumLength = 20)]
        public string Title { set; get; }
        [StringLength(int.MaxValue, ErrorMessage="Nhập tối thiểu 50 ký tự!!!", MinimumLength=50)]
        public string Body { set; get; }
        public DateTime DayCreated { set; get; }
        public DateTime DayUpdated { set; get; }
        [Required(ErrorMessage="phải nhập ký tự vào!!!")]
        public string AucountID { set; get; }

        public virtual Account Account { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

    }
}