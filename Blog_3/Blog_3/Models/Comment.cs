﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Không được để trống mục này!!!")]
        public string Body { set; get; }

        public DateTime DayCreated { set; get; }
        public DateTime DayUpdated { set; get; }
        public int PostID { set; get; }
        public string Author { set; get; }

        public virtual Post Post { set; get; }
    }
}