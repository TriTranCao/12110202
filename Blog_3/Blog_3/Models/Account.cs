﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Account
    {
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage="Vui lòng nhập địa chỉ mail!!!")]
        public string Email { set; get; }
        [StringLength(100, ErrorMessage= "nhập tối đa 100 ký tự!!!")]
        public string FirstName { set; get; }
        [Required(ErrorMessage="Bắt buộc phải nhập vào!!!")]
        [StringLength(int.MaxValue, ErrorMessage="nhập tối thiểu 50 ký tự!!!", MinimumLength=5)]
        public string LastName { set; get; }

        [Required(ErrorMessage="Bắt buộc phải nhập vào!!!")]
        public string AccountID { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}