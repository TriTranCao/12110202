﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_3.Models
{
    public class Tag
    {
        
        public int ID { set; get; }
        [Required(ErrorMessage="Bắt buộc phải nhập vào!!!")]
        [Required(ErrorMessage="Không được để trống mục này!!!")]
        [StringLength(100, ErrorMessage = "so luong ky tu 10 -100", MinimumLength = 10)]
        public string Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}