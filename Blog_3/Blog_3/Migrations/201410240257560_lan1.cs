namespace Blog_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(maxLength: 500));
            AlterColumn("dbo.Posts", "AucountID", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "AucountID", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String(maxLength: 250));
        }
    }
}
